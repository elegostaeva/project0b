#pragma once
#include <iostream>
#include <fstream>
#include <string>
class StreamReader
{
private:
	std::istream &input;
	std::string cur_line;
	int begin;
public:
	explicit StreamReader(std::istream &);

	bool has_next();

	std::string next();

	~StreamReader();
};