#include "StreamWriter.h"



StreamWriter::StreamWriter()
{
}

void StreamWriter::write(std::ostream &output, std::vector<std::pair<std::string, int>> words, int total_freaq)
{
	for (auto &j : words) {
		output << j.first << ";" << j.second << ";" << (100.f * j.second / total_freaq) << "%" << std::endl;
	}
}


StreamWriter::~StreamWriter()
{
}
