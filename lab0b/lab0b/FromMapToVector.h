#pragma once
#include <vector>
#include <string>
#include <map>
#include <algorithm>
class FromMapToVector
{
private:
	std::vector<std::pair<std::string, int>> order;
public:
	FromMapToVector();
	void vector_build(std::map<std::string, int>);
	std::vector<std::pair<std::string, int>> get_vector();
	~FromMapToVector();
};

