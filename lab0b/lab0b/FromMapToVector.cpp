#include "FromMapToVector.h"



FromMapToVector::FromMapToVector()
{
}

bool compair(std::pair<std::string, int> a, std::pair<std::string, int> b) {
	if (a.second == b.second) {
		return  (a.first < b.first);
	}
	else {
		return a.second > b.second;
	}
}

void FromMapToVector::vector_build(std::map<std::string, int> words) {
	std::vector<std::pair<std::string, int>> orders(words.begin(), words.end());
	std::sort(orders.begin(), orders.end(), compair);
	order = orders;
}

std::vector<std::pair<std::string, int>> FromMapToVector::get_vector()
{
	return order;
}

FromMapToVector::~FromMapToVector()
{
}
