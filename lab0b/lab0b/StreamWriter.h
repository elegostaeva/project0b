#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
class StreamWriter
{
public:
	StreamWriter();

	void write(std::ostream &, std::vector<std::pair<std::string, int>>, int);

	~StreamWriter();
};

