#pragma once
#include <string>
#include <map>
class MapBuilder
{
private:
	std::map<std::string, int> words;
	int total_freaq;
public:
	MapBuilder();
	void put(std::string);
	std::map<std::string, int> get_map();
	int get_total_freaq();
	~MapBuilder();
};