#include "MapBuilder.h"



MapBuilder::MapBuilder()
{
}

void MapBuilder::put(std::string word) {
	if (words[word]) {
		words[word]++;
	}
	else {
		words[word] = 1;
	}
	total_freaq++;
}

std::map<std::string, int> MapBuilder::get_map()
{
	return words;
}

int MapBuilder::get_total_freaq()
{
	return total_freaq;
}

MapBuilder::~MapBuilder()
{
}
