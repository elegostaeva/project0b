#include<iostream>
#include "StreamReader.h"
#include "MapBuilder.h"
#include "FromMapToVector.h"
#include "StreamWriter.h"
#include "header.h"
int main(int argc, char *argv[]) {
		if (argc == 1) {
			cout << "help";
			return 0;
		}
		else if ((argc == 2) && (argv[1][1] == 'h')) {
			cout << "help instructions";
			return 0;
		}
		else if ((argc == 2) && (argv[1][1] != 'h')) {
			cout << "help";
			return 0;
		}
		else if (argc > 3) {
			cout << "help";
			return 0;
		}
		ifstream input(argv[1], fstream::in);
		ofstream output(argv[2], fstream::out);
		if (!input) {
			cout << "Can't open the input file.\n\n";
			return 1;
		}
		if (!output) {
			cout << "Can't open the input file.\n\n";
			input.close();
			return 1;
		}

	//ifstream input = ifstream("input.txt");
	StreamReader reader(input);
	MapBuilder builder;
	FromMapToVector converter;
	while (reader.has_next()) {
		builder.put(reader.next());
	}
	converter.vector_build(builder.get_map());
	StreamWriter writer;
	writer.write(output, converter.get_vector(), builder.get_total_freaq());
	
	return 0;
}