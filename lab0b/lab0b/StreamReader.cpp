#include "StreamReader.h"

StreamReader::StreamReader(std::istream &in):input(in) {

}

bool StreamReader::has_next() {
	if (begin < cur_line.length()) {
		return true;
	} 
	while (getline(input, cur_line)) {
		begin = 0;
		for (char c : cur_line) {
			if (isalnum(c)) {
				return true;
			}
		}
		begin++;
	}
	return false;
}

std::string StreamReader::next() {
	bool has_result = false;
	std::string result;
	int end = begin;
	for (int i = begin; i < cur_line.length(); i++) {
		char c = cur_line[i];
		if (has_result) {
			if (isalnum(c)) {
				return result;
			}
			begin++;
		}
		else
			if (!isalnum(c)) {
				if (begin != end) {
					result = cur_line.substr(begin, end - begin);
					has_result = true;
				}
				begin = end + 1;
			}
		++end;
	}
	if (result.length() == 0) {
		result = cur_line.substr(begin, end - begin);
		begin = cur_line.length();
	}
	return result;
}
	

StreamReader::~StreamReader()
{
}
